package ga.manuelgarciacr.tripmemories;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SplashActivity extends AppCompatActivity {
    LinearLayout linear1, linear2, linear3, linear4;
    int i = 1;
    Animation fadeOut;
    ImageView mImvSplash;
    private static int SPLASH_TIME_OUT = 1900;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mImvSplash=findViewById(R.id.imvSplash);
        //mImvSplash.setAlpha(0.5f);
        Animation an= AnimationUtils.loadAnimation(this,R.anim.scale);
        mImvSplash.startAnimation(an);
        new Handler().postDelayed(() -> {
            //mImvSplash.setAlpha(1f);
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }, SPLASH_TIME_OUT);
        /*
        new Handler().postDelayed(() -> {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }, 2000);

         */
        /*
        setContentView(R.layout.activity_splash);

        linear1 = findViewById(R.id.linear1);
        linear2 = findViewById(R.id.linear2);
        linear3 = findViewById(R.id.linear3);
        linear4 = findViewById(R.id.linear4);

        fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale);
        linear1.startAnimation(fadeOut);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.e("Value of i", String.valueOf(i));
                if (i == 1) {
                    i++;
                    linear1.setBackgroundColor(Color.TRANSPARENT);
                    linear2.startAnimation(fadeOut);

                } else if (i == 2) {
                    i++;
                    linear2.setBackgroundColor(Color.TRANSPARENT);
                    linear3.startAnimation(fadeOut);

                } else if (i == 3) {
                    i++;
                    linear3.setBackgroundColor(Color.TRANSPARENT);
                    linear4.startAnimation(fadeOut);

                } else if (i == 4) {
                    i++;
                    linear4.setBackgroundColor(Color.TRANSPARENT);
                    linear1.startAnimation(fadeOut);

                } else {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        */
    }
}
